// Board.java
package oop.asg04;
/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
*/
public class Board	{
	// Some ivars are stubbed out for you:
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean DEBUG = true;
	boolean committed;

	//self-defined
	private int[] columnHeight;
    private int[] numberOfBlockInRow;
    private int maxHeight = 0;

    private boolean[][] backup_grid;
    private int[] backup_columnHeight;
    private int[] backup_numberOfBlockInRow;
    private int backup_maxHeight = 0;


	
	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		committed = true;
		// YOUR CODE HERE
        grid = new boolean[width][height];
		columnHeight = new int[width];
        numberOfBlockInRow = new int[height];

        backup_grid = new boolean[width][height];
        backup_columnHeight = new int[width];
        backup_numberOfBlockInRow = new int[height];
	}
	
	
	/**
	 Returns the width of the board in blocks.
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	*/
	public int getMaxHeight() {
        return maxHeight + 1;
		/*int result = columnHeight[0];
		for(int i = 1; i < width; i++){
			result = Math.max(result, columnHeight[i]);
		}
		return result; // YOUR CODE HERE*/
	}
	
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	*/
	public void sanityCheck() {
		if (DEBUG) {
			// YOUR CODE HERE
		}
	}
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	*/
	public int dropHeight(Piece piece, int x) {
		int[] pieceSkirt = piece.getSkirt();
		int result = columnHeight[x] - pieceSkirt[0];
		for(int i = 1; i < pieceSkirt.length; i++){
			result = Math.max(result, columnHeight[x + i] - pieceSkirt[i]);
		}
		return result + 1; // YOUR CODE HERE
	}


	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	*/
	public int getColumnHeight(int x) {
		return columnHeight[x] + 1; // YOUR CODE HERE
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	*/
	public int getRowWidth(int y) {
		 return numberOfBlockInRow[y]; // YOUR CODE HERE
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	*/
	public boolean getGrid(int x, int y) {
		return this.grid[x][y]; // YOUR CODE HERE
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	*/
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		if (!committed) throw new RuntimeException("place commit problem");

        committed = false;
		int result = PLACE_OK;
		
		// YOUR CODE HERE
        TPoint[] pieceBody = piece.getBody();
		for(int i = 0; i < pieceBody.length; i++){
            int xPiece = x + pieceBody[i].x;
            int yPiece = y + pieceBody[i].y;
            if (xPiece < 0 || xPiece >= width || yPiece < 0 || yPiece >= height){
                return  PLACE_OUT_BOUNDS;
            }

            if (grid[xPiece][yPiece]){
                return PLACE_BAD;
            }
            columnHeight[xPiece] = Math.max(columnHeight[xPiece], yPiece);
            maxHeight = Math.max(maxHeight, columnHeight[xPiece]);
            grid[xPiece][yPiece] = true;
            numberOfBlockInRow[yPiece] ++ ;
            if (numberOfBlockInRow[yPiece] == width){
                result = PLACE_ROW_FILLED;
            }
        }
		
		return result;
	}
	
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	*/
	public int clearRows() {
        committed = false;
		int rowsCleared = 0;
		// YOUR CODE HERE
		sanityCheck();

        for(int i = 0; i < height; i++){
            if (numberOfBlockInRow[i] == width){
                rowsCleared += 1;
            }
            else{
                numberOfBlockInRow[i - rowsCleared] = numberOfBlockInRow[i];
                if (rowsCleared > 0){
                    numberOfBlockInRow[i] = 0;
                }
                for(int j = 0; j < width; j++){
                    grid[j][i - rowsCleared] = grid[j][i];
                    if (rowsCleared > 0){
                        grid[j][i] = false;
                    }
                }
            }
        }

        maxHeight = 0;
        for(int i = 0; i < width; i++){
            columnHeight[i] -= rowsCleared;
            while (columnHeight[i] >= 0 && !grid[i][columnHeight[i]]){
                columnHeight[i] -- ;
            }
            maxHeight = Math.max(maxHeight, columnHeight[i]);
        }

		return rowsCleared;
	}



	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	*/
	public void undo() {
		if (committed){
            return;
        }
        //restore
        for (int i = 0; i < grid.length; i++) {
            System.arraycopy(backup_grid[i], 0, grid[i], 0, grid[0].length);
        }
        System.arraycopy(backup_columnHeight, 0, columnHeight, 0, columnHeight.length);
        System.arraycopy(backup_numberOfBlockInRow, 0, numberOfBlockInRow,
                0, numberOfBlockInRow.length);
        maxHeight = backup_maxHeight;

        committed = true; //already backup
        commit();
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
	public void commit() {
        if (!committed){
            committed = true;
            //backup
            for (int i = 0; i < grid.length; i++) {
                System.arraycopy(grid[i], 0, backup_grid[i], 0, grid[0].length);
            }
            System.arraycopy(columnHeight, 0, backup_columnHeight, 0, columnHeight.length);
            System.arraycopy(numberOfBlockInRow, 0, backup_numberOfBlockInRow,
                    0, numberOfBlockInRow.length);
            backup_maxHeight = maxHeight;
        }
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
}


