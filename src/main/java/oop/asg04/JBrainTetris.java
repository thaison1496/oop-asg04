package oop.asg04;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thaison1496 on 12/11/15.
 */
public class JBrainTetris extends JTetris{

    /**
     * Creates a new JTetris where each tetris square
     * is drawn with the given number of pixels.
     *
     * @param pixels
     */
    JBrainTetris(int pixels) {
        super(pixels);
    }

    protected JCheckBox brainMode;
    protected DefaultBrain brain = new DefaultBrain();

    //move calculated by brain when add new piece or use by adversary
    protected Brain.Move brainMove;

    //variables to save calculated position and piece
    // (Brain.Move is static and changed when used by adversary)
    protected int brainX;
    protected Piece brainPiece;

    protected JPanel little;
    protected JSlider adversary;
    protected JLabel status;

    private void enableButtons() {
        startButton.setEnabled(!gameOn);
        stopButton.setEnabled(gameOn);
    }

    @Override
    public JComponent createControlPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // COUNT
        countLabel = new JLabel("0");
        panel.add(countLabel);

        // SCORE
        scoreLabel = new JLabel("0");
        panel.add(scoreLabel);

        // TIME
        timeLabel = new JLabel(" ");
        panel.add(timeLabel);

        panel.add(Box.createVerticalStrut(12));

        // START button
        startButton = new JButton("Start");
        panel.add(startButton);
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startGame();
            }
        });

        // STOP button
        stopButton = new JButton("Stop");
        panel.add(stopButton);
        stopButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stopGame();
            }
        });

        enableButtons();

        //Brain
        panel.add(new JLabel("Brain:"));
        brainMode = new JCheckBox("Brain active");
        panel.add(brainMode);

        JPanel row = new JPanel();

        // SPEED slider
        panel.add(Box.createVerticalStrut(12));
        row.add(new JLabel("Speed:"));
        speed = new JSlider(0, 200, 75);	// min, max, current
        speed.setPreferredSize(new Dimension(100, 15));

        updateTimer();
        row.add(speed);

        panel.add(row);
        speed.addChangeListener(new ChangeListener() {
            // when the slider changes, sync the timer to its value
            public void stateChanged(ChangeEvent e) {
                updateTimer();
            }
        });

        //ADVERSARY
        // make a little panel, put a JSlider in it. JSlider responds to getValue()
        little = new JPanel();
        little.add(new JLabel("Adversary:"));
        adversary = new JSlider(0, 100, 0); // min, max, current
        adversary.setPreferredSize(new Dimension(100,15));
        little.add(adversary);

        // now add little to panel of controls
        panel.add(little);

        // add adversary status
        status = new JLabel();
        panel.add(row);
        panel.add(status);

        testButton = new JCheckBox("Test sequence");
        panel.add(testButton);


        return panel;
    }

    /**
     pick piece with adversary
     */
    public Piece pickNextPiece() {
        int randNum, pieceNum;
        randNum = (int) (adversary.getMaximum() * random.nextDouble());
        if (randNum < adversary.getValue()){
            status.setText("*ok*");
            Piece piece = pieces[0];
            double worstScore = 0;

            for(int i = 0; i < pieces.length; i++){
                brainMove = brain.bestMove(board, pieces[i], board.getHeight() - 4, null);
                if (brainMove != null && brainMove.score > worstScore){
                    worstScore = brainMove.score;
                    piece = pieces[i];
                }
            }
            return piece;
        }
        else {
            status.setText("ok");
            pieceNum = (int) (pieces.length * random.nextDouble());
            Piece piece	 = pieces[pieceNum];
            return(piece);
        }
    }

    /**
     Tries to add a new random piece at the top of the board.
     Ends the game if it's not possible.
     */
    public void addNewPiece() {
        count++;
        score++;

        if (testMode && count == TEST_LIMIT+1) {
            stopGame();
            return;
        }

        // commit things the way they are
        board.commit();
        currentPiece = null;

        Piece piece = pickNextPiece();

        //Use brain to calculate piece's position
        if (brainMode.isSelected()){
            brainMove = brain.bestMove(board, piece, board.getHeight() - 4, null);
            if (brainMove != null){
                brainX = brainMove.x;
                brainPiece = brainMove.piece;
            }
        }



        // Center it up at the top
        int px = (board.getWidth() - piece.getWidth())/2;
        int py = board.getHeight() - piece.getHeight();

        // add the new piece to be in play
        int result = setCurrent(piece, px, py);

        // This probably never happens, since
        // the blocks at the top allow space
        // for new pieces to at least be added.
        if (result>Board.PLACE_ROW_FILLED) {
            stopGame();
        }

        updateCounters();
    }

    /**
     Updates the count/score labels with the latest values.
     */
    private void updateCounters() {
        countLabel.setText("Pieces " + count);
        scoreLabel.setText("Score " + score);
    }

    /**
     Called to change the position of the current piece.
     Each key press calls this once with the verbs
     LEFT RIGHT ROTATE DROP for the user moves,
     and the timer calls it with the verb DOWN to move
     the piece down one square.

     Before this is called, the piece is at some location in the board.
     This advances the piece to be at its next location.

     Overriden by the brain when it plays.
     */

    public void tick(int verb) {
        if (!gameOn) return;

        //use brain before DOWN
        if (verb == DOWN && brainMode.isSelected() && brainMove != null){
            if (currentX > brainX)
                tick(LEFT);
            else if (currentX < brainX)
                tick(RIGHT);
            if (currentPiece != brainPiece)
                tick(ROTATE);
        }

        if (currentPiece != null) {
            board.undo();	// remove the piece from its old position
        }

        // Sets the newXXX ivars
        computeNewPosition(verb);

        // try out the new position (rolls back if it doesn't work)
        int result = setCurrent(newPiece, newX, newY);

        // if row clearing is going to happen, draw the
        // whole board so the green row shows up
        if (result ==  Board.PLACE_ROW_FILLED) {
            repaint();
        }


        boolean failed = (result >= Board.PLACE_OUT_BOUNDS);

        // if it didn't work, put it back the way it was
        if (failed) {
            if (currentPiece != null) board.place(currentPiece, currentX, currentY);
            repaintPiece(currentPiece, currentX, currentY);
        }

		/*
		 How to detect when a piece has landed:
		 if this move hits something on its DOWN verb,
		 and the previous verb was also DOWN (i.e. the player was not
		 still moving it),	then the previous position must be the correct
		 "landed" position, so we're done with the falling of this piece.
		*/
        if (failed && verb==DOWN && !moved) {	// it's landed

            int cleared = board.clearRows();
            if (cleared > 0) {
                // score goes up by 5, 10, 20, 40 for row clearing
                // clearing 4 gets you a beep!
                switch (cleared) {
                    case 1: score += 5;	 break;
                    case 2: score += 10;  break;
                    case 3: score += 20;  break;
                    case 4: score += 40; Toolkit.getDefaultToolkit().beep(); break;
                    default: score += 50;  // could happen with non-standard pieces
                }
                updateCounters();
                repaint();	// repaint to show the result of the row clearing
            }


            // if the board is too tall, we've lost
            if (board.getMaxHeight() > board.getHeight() - TOP_SPACE) {
                stopGame();
            }
            // Otherwise add a new piece and keep playing
            else {
                addNewPiece();
            }
        }

        // Note if the player made a successful non-DOWN move --
        // used to detect if the piece has landed on the next tick()
        moved = (!failed && verb!=DOWN);
    }

    public static void main(String[] args) {
        // Set GUI Look And Feel Boilerplate.
        // Do this incantation at the start of main() to tell Swing
        // to use the GUI LookAndFeel of the native platform. It's ok
        // to ignore the exception.
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) { }

        JBrainTetris tetris = new JBrainTetris(16);
        JFrame frame = JTetris.createFrame(tetris);
        frame.setVisible(true);
    }
}
